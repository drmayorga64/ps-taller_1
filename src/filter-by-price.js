import XLSX from "xlsx";
import fs from "fs/promises";
import FileWriter from "./file-writer.js"; 

/**
 * Filtra casas por precio y genera archivos XLSX y JSON con los resultados filtrados.
 *
 * @param {Object} options - Opciones de filtrado.
 * @param {Array} options.houses - Lista de casas.
 * @param {number} options.maximumPrice - Precio máximo para el filtro.
 * @param {string} options.city - Nombre de la ciudad.
 * @returns {Promise<void>}
 */
const fileWriter = new FileWriter();
async function filterByPrice({ houses, maximumPrice, city }) {
    const filteredHouses = houses
        .filter(
            (house) =>
                Number(house.priceInCLP.replace(/\D/g, "")) < maximumPrice
        )
        .map(({ location, url }) => ({ Location: location, URL: url }));

    const workSheet = XLSX.utils.json_to_sheet(filteredHouses);
    const workBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, "Houses");

    try {
		fileWriter.generateXLSX(`./xlsx/${city}.xlsx`, XLSX.write(workBook, { bookType: "xlsx", type: "buffer" }));
        console.log(`${city} XLSX File generated successfully`);

        await fs.writeFile(
            `./json/${city}.json`,
            JSON.stringify(filteredHouses)
        );
        console.log(`${city} JSON generated successfully`);
    } catch (error) {
        console.error("An error occurred:", error.message);
    }
}

export default filterByPrice;
