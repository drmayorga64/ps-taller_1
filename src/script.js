import axios from "axios";
import { load } from "cheerio";
import yargs from "yargs";
import { hideBin } from "yargs/helpers";
import filterByPrice from "./filter-by-price.js";
import FileWriter from "./file-writer.js"; 

const argv = yargs(hideBin(process.argv)).argv;

const city = argv.location || "temuco-la-araucania";
const MAX_pages = argv.maxPages || 1;	
const perPage = argv.perPage || 50;
const maximumPrice = argv.maximumPrice || null;
const delay = 1000;
argv.maximumPrice = 1000000000;
const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
const fileWriter = new FileWriter();
/**
 * Obtiene datos de casas desde un sitio web y genera un archivo JSON con los resultados.
 *
 * @returns {Promise<void>} Una promesa que se resuelve cuando se completa la generación del archivo JSON.
 */

async function getHousesFromWeb() {
    const houses = [];
    for (let page = 0; page < MAX_pages; page++) {
        console.log(`Page ${page + 1} of ${MAX_pages}`);

        const response = await axios.get(
            `https://www.portalinmobiliario.com/venta/casa/propiedades-usadas/${city}/_Desde_${perPage * page}_NoIndex_True`
        );

		// Get the HTML code of the webpage
        const html = response.data;
        const $ = load(html);

		// Find all elements with ui-search-result__wrapper class, in div element.
        $("div.ui-search-result__wrapper").each((_index, el) => {
            const section = $(el).find("div > div > a");
            const url = $(section).attr("href");
            const originalPrice = Number(
                $(section)
                    .find(".andes-money-amount__fraction")
                    .text()
                    .replace(/\./g, "")
            );
            const inUF =
                $(section).find(".andes-money-amount__currency-symbol").text() === "UF";
            const size = $(section)
                .find(".ui-search-card-attributes__attribute")
                .first()
                .text();
            const dorms = $(section)
                .find(".ui-search-card-attributes__attribute")
                .next()
                .text();
            const location = $(section)
                .children()
                .next()
                .next()
                .next()
                .children()
                .first()
                .text();
            houses.push({ url, originalPrice, inUF, size, dorms, location });
        });

        await sleep(delay);
    }

    return houses;
}

//Get houses with the method `getHousesFromWeb`, finally, get the price in CLP and generate the JSON file with the extracted data
(async () => {
    try {
        const houses = await getHousesFromWeb();

        if (houses.length === 0) {
            console.log(`No houses found in ${city}.`);
            return;
        }

        const { data } = await axios.get("https://mindicador.cl/api");

        const housesWithPriceInCLP = houses.map((house) => {
            const priceInCLP = house.inUF
                ? new Intl.NumberFormat("es-CL", {
                    currency: "CLP",
                    style: "currency",
                }).format(house.originalPrice * data.uf.valor)
                : new Intl.NumberFormat("es-CL", {
                    currency: "CLP",
                    style: "currency",
                }).format(house.originalPrice);

            return { ...house, priceInCLP };
        });


		
		fileWriter.writeFile(`./json/${city}.json`, JSON.stringify(housesWithPriceInCLP));
		
        if (argv.maximumPrice) {
            filterByPrice({
                houses: housesWithPriceInCLP,
                maximumPrice: argv.maximumPrice,
                city,
            });
        }
    } catch (error) {
        console.error("An error occurred:", error.message);
    }
})();
