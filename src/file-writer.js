import XLSX from "xlsx";
import fs from "fs/promises";

class FileWriter {
    constructor() {}

    /**
     * Escribe el contenido en un archivo.
     *
     * @param {string} fileName - Nombre del archivo.
     * @param {string} content - Contenido del archivo.
     * @returns {Promise<void>}
     */
    async writeFile(fileName, content) {
        try {
            await fs.writeFile(fileName, content);
            console.log(`${fileName} generated successfully`);
        } catch (error) {
            console.error(`Error generating ${fileName}:`, error.message);
        }
    }

    /**
     * Genera un archivo XLSX a partir de una matriz de datos.
     *
     * @param {string} fileName - Nombre del archivo XLSX.
     * @param {Array} data - Datos para el archivo XLSX.
     * @param {string} sheetName - Nombre de la hoja en el archivo XLSX.
     * @returns {Promise<void>}
     */
    async generateXLSX(fileName, data, sheetName = "Sheet1") {
        const workSheet = XLSX.utils.json_to_sheet(data);
        const workBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workBook, workSheet, sheetName);
        const xlsxContent = XLSX.write(workBook, { bookType: "xlsx", type: "buffer" });
        await this.writeFile(fileName, xlsxContent);
    }
}

export default FileWriter;
