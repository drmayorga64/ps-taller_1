
# Taller 1 - Pruebas de software

Integrantes: Armin Rodriguez, Diego Mayorga.

### Install libraries

`npm install`

### IMPORTANT

**Please, first create the `json` and `xlsx` folder in the root of the project**

### Run with default configuration

`npm run start`
